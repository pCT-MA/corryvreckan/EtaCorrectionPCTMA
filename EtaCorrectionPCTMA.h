#pragma once

#include "core/module/Module.hpp"

class TH1F;

namespace corryvreckan {
    class EtaCorrectionPCTMA : public Module {
    public:
        EtaCorrectionPCTMA(Configuration& config, std::shared_ptr<Detector> detector);
        virtual ~EtaCorrectionPCTMA() {}

        void initialize() override;

        StatusCode run(std::shared_ptr<Clipboard> const&) override;

    private:
        std::shared_ptr<Detector> mDetector;
        // 0: hybrid-wise histograms, 1-6: apv-wise histograms
        std::array<TH1F*, 7> mEtaXHistograms;
        std::array<TH1F*, 7> mEtaYHistograms;
        // whether to apply the correction or to measure/calculate it
        bool mApplyCorrection;
        int mNumberOfBinsHybrid;
        int mNumberOfBinsApv;
        double mClusterWidthX;
        double mClusterWidthY;

        bool LoadCumulativeHistograms(std::string const& filename);
        TH1* mEtaXCumulative;
        TH1* mEtaYCumulative;
        int mNumberOfBinsCorrected;
        TH1F* mXCorrectedHistogram;
        TH1F* mYCorrectedHistogram;
    };
} // namespace corryvreckan

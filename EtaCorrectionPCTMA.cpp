#include <vector>

#include <core/utils/file.h>

#include "EtaCorrectionPCTMA.h"

#include <TFile.h>
#include <TH1.h>

namespace corryvreckan {

    EtaCorrectionPCTMA::EtaCorrectionPCTMA(Configuration& config, std::shared_ptr<Detector> detector)
        : Module(config, detector), mDetector(detector), mEtaXHistograms{}, mEtaYHistograms{}, mApplyCorrection(false),
          mClusterWidthX(0.f), mClusterWidthY(0.f), mEtaXCumulative(nullptr), mEtaYCumulative(nullptr),
          mNumberOfBinsCorrected(50), mXCorrectedHistogram(nullptr), mYCorrectedHistogram(nullptr) {

        auto path_to_absolute = [&config](std::string path) {
            if(path.front() != '/') {
                std::string directory = config.getFilePath().substr(0, config.getFilePath().find_last_of('/'));
                path = directory + "/" + path;
            }
            return path;
        };

        std::string correctionFileName = path_to_absolute(config.get<std::string>("config_file", ""));
        if(corryvreckan::path_is_file(correctionFileName) && LoadCumulativeHistograms(correctionFileName)) {
            mApplyCorrection = true;
        }

        mClusterWidthX = config.get<float>("cluster_width_x", 0.f);
        mClusterWidthY = config.get<float>("cluster_width_y", 0.f);
        mNumberOfBinsHybrid = config.get<int>("number_of_bins_hybrid", 1000);
        mNumberOfBinsApv = config.get<int>("number_of_bins_apv", 50);
        mNumberOfBinsCorrected = config.get<int>("number_of_bins_corrected", 50);
    }

    // shut up -Wmissing-declarations
    std::string ConstructNameHybrid(std::string const& detectorId, bool x);
    std::string ConstructNameHybrid(std::string const& detectorId, bool x) {
        return "eta_" + detectorId + "_" + (x ? "x" : "y");
    }
    std::string ConstructTitleHybrid(std::string const& detectorId, bool x);
    std::string ConstructTitleHybrid(std::string const& detectorId, bool x) {
        return "Eta (" + detectorId + "_" + (x ? "x" : "y") + ");#eta = CoG - floor(CoG);N";
    }
    std::string ConstructNameApv(std::string const& detectorId, bool x, std::size_t apv);
    std::string ConstructNameApv(std::string const& detectorId, bool x, std::size_t apv) {
        return "eta_" + detectorId + "_" + (x ? "x" : "y") + "_apv_" + std::to_string(apv);
    }
    std::string ConstructTitleApv(std::string const& detectorId, bool x, std::size_t apv);
    std::string ConstructTitleApv(std::string const& detectorId, bool x, std::size_t apv) {
        return "Eta (" + detectorId + "_" + (x ? "x" : "y") + ", APV: " + std::to_string(apv) +
               ");#eta = CoG - floor(CoG);N";
    }

    void EtaCorrectionPCTMA::initialize() {
        auto spawnHistogram = [](std::string const& name, std::string const& title, int bins) {
            return new TH1F(name.c_str(), title.c_str(), bins, 0, 1);
        };
        auto spawnHistogramHybrid = [spawnHistogram, this](std::string const& detectorId, bool x) {
            return spawnHistogram(
                ConstructNameHybrid(detectorId, x).c_str(), ConstructTitleHybrid(detectorId, x), mNumberOfBinsHybrid);
        };
        auto spawnHistogramApv = [spawnHistogram, this](std::string const& detectorId, bool x, std::size_t apv) {
            return spawnHistogram(
                ConstructNameApv(detectorId, x, apv), ConstructTitleApv(detectorId, x, apv), mNumberOfBinsApv);
        };

        if(!mApplyCorrection) {
            const auto detectorId = mDetector->getName();
            mEtaXHistograms[0] = spawnHistogramHybrid(detectorId, true);
            mEtaYHistograms[0] = spawnHistogramHybrid(detectorId, false);
            for(std::size_t i = 0; i + 1 < mEtaXHistograms.size(); i++) {
                mEtaXHistograms[i + 1] = spawnHistogramApv(detectorId, true, i);
            }
            for(std::size_t i = 0; i + 1 < mEtaYHistograms.size(); i++) {
                mEtaYHistograms[i + 1] = spawnHistogramApv(detectorId, false, i);
            }
        } else {
            mEtaXCumulative->SetDirectory(getROOTDirectory());
            mEtaYCumulative->SetDirectory(getROOTDirectory());
            // debugging correction
            mXCorrectedHistogram = spawnHistogram(
                "x_corrected", "pdf of #tilde{x};#tilde{x} = floor(x) + pitch.x * F(#eta);N", mNumberOfBinsCorrected);
            mYCorrectedHistogram = spawnHistogram(
                "y_corrected", "pdf of #tilde{y};#tilde{y} = floor(y) + pitch.y * F(#eta);N", mNumberOfBinsCorrected);
        }
    }

    StatusCode EtaCorrectionPCTMA::run(std::shared_ptr<Clipboard> const& cb) {
        auto etaValue = [](double x) { return x - std::floor(x); };
        auto& clusters = cb->getData<Cluster>(mDetector->getName());

        auto condition = [](double v, double compare) { return compare == 0.f || v == compare; };

        if(!mApplyCorrection) {
            for(const auto& cluster : clusters) {
                if(condition(cluster->rowWidth(), mClusterWidthX)) {
                    const auto etaX = etaValue(cluster->row());
                    mEtaXHistograms.at(0)->Fill(etaX);
                    mEtaXHistograms.at(1 + static_cast<std::size_t>(std::floor(cluster->row() / 128.)))->Fill(etaX);
                }
                if(condition(cluster->columnWidth(), mClusterWidthY)) {
                    const auto etaY = etaValue(cluster->column());
                    mEtaYHistograms.at(0)->Fill(etaY);
                    mEtaYHistograms.at(1 + static_cast<std::size_t>(std::floor(cluster->column() / 128.)))->Fill(etaY);
                }
            }
        } else {
            auto integral = [](TH1* histogram, double eta) {
                return histogram->GetBinContent(histogram->FindBin(eta)) / histogram->GetMaximum();
            };

            for(auto& cluster : clusters) {
                double row = cluster->row();
                double col = cluster->column();

                if(condition(cluster->rowWidth(), mClusterWidthX)) {
                    const auto ix = integral(mEtaXCumulative, etaValue(cluster->row()));
                    row = std::floor(row) + ix;
                    mXCorrectedHistogram->Fill(ix);
                }
                if(condition(cluster->columnWidth(), mClusterWidthY)) {
                    const auto iy = integral(mEtaYCumulative, etaValue(cluster->column()));
                    col = std::floor(col) + iy;
                    mYCorrectedHistogram->Fill(iy);
                }
                const auto local = mDetector->getLocalPosition(col, row);
                const auto global = mDetector->localToGlobal(local);
                cluster->setRow(row);
                cluster->setColumn(col);
                cluster->setClusterCentreLocal(local);
                cluster->setClusterCentre(global);
            }
        }
        return clusters.empty() ? StatusCode::NoData : StatusCode::Success;
    }

    bool EtaCorrectionPCTMA::LoadCumulativeHistograms(std::string const& filename) {
        TFile correctionFile(filename.c_str(), "READ");
        if(correctionFile.IsOpen()) {
            const auto detectorId = mDetector->getName();
            const auto baseName = "EtaCorrectionPCTMA/" + detectorId + "/";
            auto hx = dynamic_cast<TH1F*>(correctionFile.Get((baseName + ConstructNameHybrid(detectorId, true)).c_str()));
            auto hy = dynamic_cast<TH1F*>(correctionFile.Get((baseName + ConstructNameHybrid(detectorId, false)).c_str()));
            mEtaXCumulative = hx != nullptr ? hx->GetCumulative() : nullptr;
            mEtaYCumulative = hy != nullptr ? hy->GetCumulative() : nullptr;
            // Until this _smart_ nonsense is fixed in root, we have to perform
            // some black magic:
            // > When you close a TFile, all histograms associated with this
            // > file are deleted. Remember that, by default, histograms are
            // > created in the current directory(in your case the TFile)
            mEtaXCumulative->SetDirectory(nullptr);
            mEtaYCumulative->SetDirectory(nullptr);
            correctionFile.Close();
            return mEtaXCumulative != nullptr && mEtaYCumulative != nullptr;
        }
        return false;
    }
} // namespace corryvreckan
